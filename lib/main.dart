import 'package:flutter/material.dart';
import 'package:gofullthrottle/services/global_service.dart';
import 'package:provider/provider.dart';

import 'app/app.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<GlobalService>(create: (_) => GlobalService())
        ],
        child: MaterialApp(
          home: App(),
        ));
  }
}
