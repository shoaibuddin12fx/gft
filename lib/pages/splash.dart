import 'package:flutter/material.dart';
import 'package:gofullthrottle/app/app_routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //
    //
    Future.delayed(Duration(seconds: 5), () {
      // 5 seconds over, navigate to Page2.
      Navigator.of(context)
          .pushNamedAndRemoveUntil(AppRoutes.APP_LOGIN, (route) => false);
    });

    //
    //
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Image.asset(
        "assets/images/carbg.jpg",
        fit: BoxFit.cover,
        height: double.infinity,
        width: double.infinity,
        alignment: Alignment.center,
      ),
    );
  }
}
