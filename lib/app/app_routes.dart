import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gofullthrottle/pages/dashboard.dart';
import 'package:gofullthrottle/pages/forget_password.dart';
import 'package:gofullthrottle/pages/login.dart';
import 'package:gofullthrottle/pages/signup.dart';
import 'package:gofullthrottle/pages/splash.dart';

class AppRoutes {
  static const String APP_SPLASH = "/splash";
  static const String APP_LOGIN = "/login";
  static const String APP_SIGNUP = "/signup";
  static const String APP_FORGET_PASSWORD = "/forget_password";
  static const String APP_DASHBOARD = "/dashboard";

  Route getRoutes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case APP_SPLASH:
        {
          return MaterialPageRoute<void>(
              builder: (BuildContext context) => SplashScreen());
        }

      case APP_LOGIN:
        {
          return MaterialPageRoute<void>(
              builder: (BuildContext context) => LoginScreen());
        }
      case APP_SIGNUP:
        {
          return MaterialPageRoute<void>(
              builder: (BuildContext context) => SignupScreen());
        }
      case APP_FORGET_PASSWORD:
        {
          return MaterialPageRoute<void>(
              builder: (BuildContext context) => ForgetPasswordScreen());
        }
      case APP_DASHBOARD:
        {
          return MaterialPageRoute<void>(
              builder: (BuildContext context) => DashboardScreen());
        }
      default:
        {
          return MaterialPageRoute<void>(
              builder: (BuildContext context) => SplashScreen());
        }
    }
  }
}
