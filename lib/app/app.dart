import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:gofullthrottle/app/app_routes.dart';

class App extends StatelessWidget {
  AppRoutes getAppRoutes() {
    return AppRoutes();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: getAppRoutes().getRoutes,
    );
  }
}
